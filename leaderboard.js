﻿GameStates.LeaderBoard = function (game) {
    //variables
};

GameStates.LeaderBoard.prototype = {

    create: function (game) {
        //button sound click
        buttonSound = game.add.audio('buttonSound');
        buttonSound.allowMultiple = true;
        //create background
        game.add.image(game.world.centerX, game.world.centerY, 'leaderBoardBackground').anchor.set(0.5);
        //create menu button 
        menuButton = game.add.button(768, 640, 'menuButton', this.stopGame, this, 0, 0, 1);
        menuButton.anchor.setTo(0.5, 0.5);
        menuButton.input.useHandCursor = true;
 
        var score = localStorage.getItem('score');
        var scoretext = game.add.text(game.world.centerX, game.world.centerY + 64, ' ', { font: "25px Arial", fill: "#FFF", align: "center" });
        scoretext.anchor.setTo(0.5, 0.5);

        var highScoresJson;
        var highScoresArray = [];
        if (localStorage.getItem('highScores') != null) {
            highScoresJson = localStorage.getItem('highScores');
            //maak array van json (scoreArray)
            highScoresArray = JSON.parse(highScoresJson);
        }
        else {
            localStorage.setItem('highScores', 0); score = 0;
        }
        //sorteer functie
        function sortNumber(a, b) {
            return b - a;
        }
        highScoresArray.sort(sortNumber);
        var userName = "test";
        var testString = "";
        //for (var i = 0; i < highScoresArray.length; i++) {
        for (var i = 0; i < 8; i++) {
            testString += highScoresArray[i] + "                             " + userName + "\r\n"
        }
        console.log("testString: " + testString);
        var headText = game.add.text(game.world.centerX, 256, ' ', { font: "bold 25px Arial", fill: "#FFF", align: "center" });
        headText.anchor.setTo(0.5, 0.5);
        headText.setText('Highscore               Naam');
        scoretext.setText(testString);

        //https://www.w3schools.com/js/tryit.asp?filename=tryjs_array_associative_2 

        //naam toevoegen aan json
        //sprite vóór het leaderboard: naam invoeren
    },

    stopGame: function (game) {
        buttonSound.play();
        //transition properties
        this.game.stateTransition.configure({
            duration: Phaser.Timer.SECOND * 0.5,
            ease: Phaser.Easing.Linear.InOut,
            properties: {
                alpha: 0,
            }
        });
        //ga naar MainMenu
        this.game.stateTransition.to('MainMenu', true, false);
    }
};