﻿GameStates.MainMenu = function (game) {

};

GameStates.MainMenu.prototype = {
    create: function (game) {
        // create main menu text and images -
        // create a "Start Game" mechanism - variety of ways to do this...

        //add audio
        buttonSound = game.add.audio('buttonSound');
        buttonSound.allowMultiple = true;
        //create background
        game.add.image(game.world.centerX, game.world.centerY, 'mainMenuBackground').anchor.set(0.5);
        //play game button
        playButton = game.add.button(game.world.centerX, game.world.centerY, 'playButton', this.playGame, this, 0, 0, 1);
        playButton.anchor.setTo(0.5, 0.5);
        playButton.input.useHandCursor = true;
        //leader board button
        leaderBoardButton = game.add.button(256, 640, 'leaderBoardButton', this.leaderBoard, this, 0, 0, 1);
        leaderBoardButton.anchor.setTo(0.5, 0.5);
        leaderBoardButton.input.useHandCursor = true;
        //keyboard input Enter
        this.enterKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.enterKey.onDown.add(this.playGame, this);
    },

    playGame: function (game) {
        //play button click
        buttonSound.play();
        //transition properties
        this.game.stateTransition.configure({
            duration: Phaser.Timer.SECOND * 0.5,
            ease: Phaser.Easing.Linear.InOut,
            properties: {
                alpha: 0,
                scale: {
                    x: 1.4,
                    y: 1.4
                }
            }
        });
        //ga naar level01
        this.game.stateTransition.to('Level01', true, false);
    },

    leaderBoard: function (game) {
        //play button click
        buttonSound.play();
        //transition properties
        this.game.stateTransition.configure({
            duration: Phaser.Timer.SECOND * 0.5,
            ease: Phaser.Easing.Linear.InOut,
            properties: {
                alpha: 0,
            }
        });
        // ga naar leaderboard
        this.game.stateTransition.to('LeaderBoard', true, false);
    }
};