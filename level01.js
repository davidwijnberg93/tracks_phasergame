﻿GameStates.Level01 = function (game) {
    
};
var score = 0;
var score2 = 0;
var scoreText2;
var totaalScore;

GameStates.Level01.prototype = {

    create: function (game) {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        //variables
        var AnimDirection = {
            North: 0,
            South: 1,
            East: 2,
            West: 3,
        }
        var playButton;
        var retryButton;
        var circle;
        var cornerRotating = false;
        var circleMoving = false;
        var counter = 3;
        var text = 0;
        var tileArray = [];
        var bonusArray = [];
        var fx;
        var score = localStorage.setItem('score', 0);
        var level = 1;
        var scoreText;

        //create fx and sounds
        cornerRotateSound = game.add.audio('cornerRotateSound');
        cornerRotateSound.allowMultiple = true;
        countdownSound = game.add.audio('countdownSound');
        countdownSound.allowMultiple = true;
        gameOverSound = game.add.audio('gameOverSound');
        gameOverSound.allowMultiple = true;
        buttonSound = game.add.audio('buttonSound');
        buttonSound.allowMultiple = true;
        winSound = game.add.audio('winSound');
        winSound.allowMultiple = true;

        //create background
        game.add.image(game.world.centerX, game.world.centerY, 'Background').anchor.set(0.5);
        //create tile board
        var tileGroup = game.add.group();
        var tile;
        var rows = 5;
        var columns = 5;
        for (var i = 0; i < columns; i++) {
            for (var j = 0; j < rows; j++) {
                tile = tileGroup.create(192 + (128 * i), 64 + (128 * j), 'Tile');
            }
        }

        //create corners + click + push to tileArray
        corner = game.add.sprite(384, 384, 'Corner');
        corner.anchor.setTo(0.5, 0.5);
        corner.angle += 180;
        corner.inputEnabled = true;
        corner.events.onInputDown.add(() => rotateCorner(corner), this);
        corner.input.useHandCursor = true;
        tileArray.push(corner);

        corner2 = game.add.sprite(256, 384, 'Corner');
        corner2.anchor.setTo(0.5, 0.5);
        corner2.angle += 0;
        corner2.inputEnabled = true;
        corner2.events.onInputDown.add(() => rotateCorner(corner2), this);
        corner2.input.useHandCursor = true;
        tileArray.push(corner2);

        corner3 = game.add.sprite(384, 512, 'Corner');
        corner3.anchor.setTo(0.5, 0.5);
        corner3.angle += 270;
        corner3.inputEnabled = true;
        corner3.events.onInputDown.add(() => rotateCorner(corner3), this);
        corner3.input.useHandCursor = true;
        tileArray.push(corner3);

        corner4 = game.add.sprite(384, 256, 'Corner');
        corner4.anchor.setTo(0.5, 0.5);
        corner4.angle += 90;
        corner4.inputEnabled = true;
        corner4.events.onInputDown.add(() => rotateCorner(corner4), this);
        corner4.input.useHandCursor = true;
        tileArray.push(corner4);

        corner5 = game.add.sprite(512, 256, 'Corner');
        corner5.anchor.setTo(0.5, 0.5);
        corner5.angle += 90;
        corner5.inputEnabled = true;
        corner5.events.onInputDown.add(() => rotateCorner(corner5), this);
        corner5.input.useHandCursor = true;
        tileArray.push(corner5);

        corner6 = game.add.sprite(512, 128, 'Corner');
        corner6.anchor.setTo(0.5, 0.5);
        corner6.angle += 90;
        corner6.inputEnabled = true;
        corner6.events.onInputDown.add(() => rotateCorner(corner6), this);
        corner6.input.useHandCursor = true;
        tileArray.push(corner6);

        corner7 = game.add.sprite(640, 256, 'Corner');
        corner7.anchor.setTo(0.5, 0.5);
        corner7.angle += 270;
        corner7.inputEnabled = true;
        corner7.events.onInputDown.add(() => rotateCorner(corner7), this);
        corner7.input.useHandCursor = true;
        tileArray.push(corner7);

        corner8 = game.add.sprite(640, 128, 'Corner');
        corner8.anchor.setTo(0.5, 0.5);
        corner8.angle += 0;
        corner8.inputEnabled = true;
        corner8.events.onInputDown.add(() => rotateCorner(corner8), this);
        corner8.input.useHandCursor = true;
        tileArray.push(corner8);

        corner9 = game.add.sprite(768, 128, 'Corner');
        corner9.anchor.setTo(0.5, 0.5);
        corner9.angle += 270;
        corner9.inputEnabled = true;
        corner9.events.onInputDown.add(() => rotateCorner(corner9), this);
        corner9.input.useHandCursor = true;
        tileArray.push(corner9);

        corner10 = game.add.sprite(768, 256, 'Corner');
        corner10.anchor.setTo(0.5, 0.5);
        corner10.angle += 180;
        corner10.inputEnabled = true;
        corner10.events.onInputDown.add(() => rotateCorner(corner10), this);
        corner10.input.useHandCursor = true;
        tileArray.push(corner10);

        corner11 = game.add.sprite(768, 384, 'Corner');
        corner11.anchor.setTo(0.5, 0.5);
        corner11.angle += 270;
        corner11.inputEnabled = true;
        corner11.events.onInputDown.add(() => rotateCorner(corner11), this);
        corner11.input.useHandCursor = true;
        tileArray.push(corner11);

        corner12 = game.add.sprite(640, 384, 'Corner');
        corner12.anchor.setTo(0.5, 0.5);
        corner12.angle += 90;
        corner12.inputEnabled = true;
        corner12.events.onInputDown.add(() => rotateCorner(corner12), this);
        corner12.input.useHandCursor = true;
        tileArray.push(corner12);

        corner13 = game.add.sprite(640, 512, 'Corner');
        corner13.anchor.setTo(0.5, 0.5);
        corner13.angle += 270;
        corner13.inputEnabled = true;
        corner13.events.onInputDown.add(() => rotateCorner(corner13), this);
        corner13.input.useHandCursor = true;
        tileArray.push(corner13);

        corner14 = game.add.sprite(512, 512, 'Corner');
        corner14.anchor.setTo(0.5, 0.5);
        corner14.angle += 180;
        corner14.inputEnabled = true;
        corner14.events.onInputDown.add(() => rotateCorner(corner14), this);
        corner14.input.useHandCursor = true;
        tileArray.push(corner14);

        corner15 = game.add.sprite(512, 640, 'Corner');
        corner15.anchor.setTo(0.5, 0.5);
        corner15.angle += 270;
        corner15.inputEnabled = true;
        corner15.events.onInputDown.add(() => rotateCorner(corner15), this);
        corner15.input.useHandCursor = true;
        tileArray.push(corner15);

        corner16 = game.add.sprite(384, 640, 'Corner');
        corner16.anchor.setTo(0.5, 0.5);
        corner16.angle += 90;
        corner16.inputEnabled = true;
        corner16.events.onInputDown.add(() => rotateCorner(corner16), this);
        corner16.input.useHandCursor = true;
        tileArray.push(corner16);

        corner17 = game.add.sprite(768, 512, 'Corner');
        corner17.anchor.setTo(0.5, 0.5);
        corner17.angle += 180;
        corner17.inputEnabled = true;
        corner17.events.onInputDown.add(() => rotateCorner(corner17), this);
        corner17.input.useHandCursor = true;
        tileArray.push(corner17);

        corner18 = game.add.sprite(256, 512, 'Corner');
        corner18.anchor.setTo(0.5, 0.5);
        corner18.angle += 180;
        corner18.inputEnabled = true;
        corner18.events.onInputDown.add(() => rotateCorner(corner18), this);
        corner18.input.useHandCursor = true;
        tileArray.push(corner18);

        corner19 = game.add.sprite(256, 640, 'Corner');
        corner19.anchor.setTo(0.5, 0.5);
        corner19.angle += 90;
        corner19.inputEnabled = true;
        corner19.events.onInputDown.add(() => rotateCorner(corner19), this);
        corner19.input.useHandCursor = true;
        tileArray.push(corner19);

        //create bonus circles
        bonusGroup = game.add.group();
        bonusGroup.enableBody = true;

        var bonus1 = bonusGroup.create(492, 620, 'Bonus');
        bonus1.anchor.setTo(0.5, 0.5);
        bonus1.scale.setTo(1.5, 1.5);
        bonusArray.push(bonus1);

        var bonus2 = bonusGroup.create(404, 276, 'Bonus');
        bonus2.anchor.setTo(0.5, 0.5);
        bonus2.scale.setTo(1.5, 1.5);
        bonusArray.push(bonus2);

        var bonus3 = bonusGroup.create(748, 364, 'Bonus');
        bonus3.anchor.setTo(0.5, 0.5);
        bonus3.scale.setTo(1.5, 1.5);
        bonusArray.push(bonus3);

        //create static elements
        straight = game.add.sprite(256, 256, 'Straight');
        straight.angle += 90;
        straight.anchor.setTo(0.5, 0.5);
        start = game.add.sprite(256, 128, 'Home');
        start.anchor.setTo(0.5, 0.5);
        finish = game.add.sprite(768, 640, 'Finish');
        finish.anchor.setTo(0.5, 0.5);
        //levelsprite = game.add.image(70, 90, 'Level01');
        //levelsprite.anchor.setTo(0.5, 0.5);

        //hide corners + straight before click start button
        for (i = 0; i < tileArray.length ; i++) {
            tileArray[i].visible = false;
            tileArray[i].alpha = 0;
        }
        for (i = 0; i < bonusArray.length ; i++) {
            bonusArray[i].visible = false;
            bonusArray[i].alpha = 0;
        }
        straight.visible = false;
        straight.alpha = 0;
        //levelsprite.visible = false;
        //levelsprite.alpha = 0;

        //instructie overlay???
        //beginOverlay = game.add.sprite(0, 0, 'beginOverlay');
        //beginOverlay.alpha = 1;
        //playButton = game.add.button(game.world.centerX, game.world.centerY, 'playButton', beginGame, this, 0, 0, 1);
        //playButton.anchor.setTo(0.5, 0.5);
        //playButton.input.useHandCursor = true;
        //playButton.alpha = 1;

        levelSprite = game.add.sprite(-10, -10, 'levelSprite');
        //create in-game level en score text
        levelText = game.add.text(20, 30, 'Level ' + level, { fontSize: '25px', fill: '#50514F' });
        scoreText = game.add.text(20, 64, 'Score ' + localStorage.getItem('score'), { fontSize: '25px', fill: '#50514F' });

        //create circle
        player = game.add.sprite(256, 28, 'Circle');
        player.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(player);

        scoreText2 = this.game.add.text(16, 145, 'Bonus score 0', { fontSize: '16px', fill: '#000' });
        totaalScore = this.game.add.text(16, 175, 'Totaal score 0 ' + score2, { fontSize: '16px', fill: '#000' });

        //ga naar functie beginGame
        beginGame();

        function rotateCorner(cornerRotation) {
            //rotate corner
            if (!cornerRotating) {
                cornerRotating = true;
                cornerRotateSound.play();
                let newRotation = cornerRotation.angle + 90;
                let tween = game.add.tween(cornerRotation).to({ angle: newRotation }, 300, Phaser.Easing.Sinusoidal.InOut, true);
                tween.onComplete.add(onCompleteCornerRotate, this);
            }
        }

        function onCompleteCornerRotate() {
            //complete corner rotate tween
            cornerRotating = false;
        }

        function beginGame() {
            //play audio
            buttonSound.play();
            countdownSound.play();

            //instructie overlay hide?
            //alpha tween overlay + button destroy
            //game.add.tween(beginOverlay).to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true);
            //playButton.destroy();

            //make corners and straight visible + alpha tween
            for (i = 0; i < tileArray.length ; i++) {
                tileArray[i].visible = true;
                game.add.tween(tileArray[i]).to({ alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 1000);
            }
            for (i = 0; i < bonusArray.length ; i++) {
                bonusArray[i].visible = true;
                game.add.tween(bonusArray[i]).to({ alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 1000);
            }
            straight.visible = true;
            game.add.tween(straight).to({ alpha: 1 }, 2000, Phaser.Easing.Linear.None, true, 1000);
            
            //alpha tween level sprite
            //create level sprite
            //levelsprite.visible = true;
            //game.add.tween(levelsprite).to({ alpha: 1 }, 2000, Phaser.Easing.Linear.None, true);

            //counter create + countertext
            text.visible = true;
            text.alpha = 1;
            text = game.add.text(game.world.centerX, game.world.centerY, '3', { font: "130px Arial", fill: "#f2e05c", align: "center" });
            text.stroke = '#50514f';
            text.strokeThickness = 4;
            text.anchor.setTo(0.5, 0.5);
            game.time.events.loop(Phaser.Timer.SECOND, countDown, this);
            game.add.tween(text).to({ alpha: 0 }, 1000, "Linear", true, 2000);
            game.add.tween(text.scale).to({ x: 2, y: 2 }, 3000, Phaser.Easing.Linear.None, true);
            //go to function circlebeginbounce
            circleBeginBounce()
        }

        function countDown() {
            //countdown function
            counter--;
            countdownSound.play();
            text.setText(counter);
            if (counter < 1) {
                text.visible = false;
                countdownSound.stop();
            }
        }

        function circleBeginBounce() {
            //create circlebounce tween
            player.alpha = 0;
            game.add.tween(player).to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 500);
            game.add.tween(player).to({ x: 256, y: 128 }, 1500, 'Bounce', true, 500).start();
            //na 3 seconde, ga naar circleBeginTween
            setTimeout(circleBeginTween, 3000);
        }

        function circleBeginTween() {
            //circle moving oncomplete
            if (!circleMoving) {
                circleMoving = true;
                //var svg path   
                var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
                //tween point vars
                var mX = 256;
                var mY = 128;
                var eX = 256;
                var eY = 320;
                //create points
                var invulString = "M" + mX + " " + mY + " " + "L" + eX + " " + eY;
                path.setAttribute('d', invulString)
                //
                var totalPathLength = path.getTotalLength();
                var startPoint = path.getPointAtLength(0)
                //draw the path
                debugPath = game.add.graphics(0, 0)
                debugPath.lineStyle(0, 0xff0000)
                debugPath.moveTo(startPoint.x, startPoint.y)
                //a little tween helper so we can tween a single variable
                tweenHelper = { progress: 0 }
                tweenHelper.onUpdate = function (tween, value) {
                    p = path.getPointAtLength(totalPathLength * value)
                    player.position.x = p.x
                    player.position.y = p.y
                    debugPath.lineTo(p.x, p.y)
                }
                //tween circle
                tween = game.add.tween(tweenHelper).to({ progress: 1 }, 1500, "Linear", true, 0).start();
                tween.onUpdateCallback(tweenHelper.onUpdate);
                //after tween, go to function getNext tile
                tween.onComplete.add(() => GetNextTile(eX, eY, mX, mY));
            }
        }

        function GetNextTile(endPointX, endPointY, middlePointX, middlePointY) {
            //circle moving oncomplete + circle destroy
            circleMoving = false;
            //variables
            var xPosCircle = endPointX;
            var yPosCircle = endPointY;
            var xPosNextTile;
            var yPosNextTile;
            var circleRichting;
            //if else statemenet getnexttile > waar komt de circle vandaan, dus welke richting gaat hij op
            //-Eindpunt op de tile waarover ik net de animatie heb laten lopen
            //-Middelpunt van de tile waarover ik net de animatie heb laten lopen
            //-middelpunt en eindpunt van oude tile berekenen de richting waar de cirkel opgaat
            if (xPosCircle == middlePointX) {
                //boven naar onder
                if (yPosCircle > middlePointY) {
                    xPosNextTile = xPosCircle;
                    yPosNextTile = yPosCircle + 64;
                    circleRichting = AnimDirection.South;
                } else {
                    //onder naar boven
                    xPosNextTile = xPosCircle;
                    yPosNextTile = yPosCircle - 64;
                    circleRichting = AnimDirection.North;
                }
            } else {
                //links naar rechts
                if (xPosCircle > middlePointX) {
                    xPosNextTile = xPosCircle + 64;
                    yPosNextTile = yPosCircle;
                    circleRichting = AnimDirection.East;
                } else {
                    //rechts naar links
                    xPosNextTile = xPosCircle - 64;
                    yPosNextTile = yPosCircle;
                    circleRichting = AnimDirection.West;
                }
            }
            //nu weten we de richting waar de cirkel heen gaat (north, south, east of west)

            //zoek de volgende corner in tileArray, met behulp van het eindpunt (x,y) van de circle na de laatst uitgevoerde tween.
            for (i = 0; i < tileArray.length; i++) {
                //komt het eindpunt (x,y) van de circle overheen met de volgende corner? zoek in tileArray welke corner dit is
                if (xPosNextTile == tileArray[i].world.x && yPosNextTile == tileArray[i].world.y) {   //Als ik de goede corner gevonden heb: Kijk naar de angle van de volgende corner.
                    var tileAngle = tileArray[i].angle;
                    //ga naar function GetAnimTween, geef de angle van de corner, de direction van de circle en de x en y positite van de corner mee
                    GetAnimTween(tileAngle, circleRichting, tileArray[i].world.x, tileArray[i].world.y, xPosCircle, yPosCircle);
                    break;
                } else if (i == tileArray.length - 1) {
                    //als de volgende corner niet gevonden kan worden: foutmelding + fx geen corner op next tile/rand van bord: ga naar function Gameover
                    boardEdgeGameOver(xPosCircle, yPosCircle);
                } else if (xPosNextTile == finish.world.x && yPosNextTile == finish.world.y) {
                    //level complete
                    levelComplete(xPosCircle, yPosCircle);
                }
            }
        }

        function GetAnimTween(angle, direction, nextTileX, nextTileY, xPosCircle, yPosCircle) {
            //eerst kijken naar de volgende angle, dan naar de volgende direction en daarna de juiste tween of foutmelding erbij zoeken
            switch (angle) {
                case 0:
                    if (direction == AnimDirection.North) {
                        //corner angle staat verkeerd om tween te kunnen uitvoeren: ga naar function Gameover
                        gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY);
                    } else if (direction == AnimDirection.East) {
                        //corner angle staat verkeerd om tween te kunnen uitvoeren: ga naar function Gameover
                        gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY);
                    } else if (direction == AnimDirection.South) {
                        //voer tween uit
                        upRight(tileArray[i].world.x, tileArray[i].world.y);
                    } else if (direction == AnimDirection.West) {
                        //voer tween uit
                        rightUp(tileArray[i].world.x, tileArray[i].world.y);
                    }
                    break;
                case 90:
                    if (direction == AnimDirection.North) {
                        //voer tween uit
                        downRight(tileArray[i].world.x, tileArray[i].world.y);
                    } else if (direction == AnimDirection.East) {
                        //corner angle staat verkeerd om tween te kunnen uitvoeren: ga naar function Gameover
                        gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY);
                    } else if (direction == AnimDirection.South) {
                        //corner angle staat verkeerd om tween te kunnen uitvoeren: ga naar function Gameover
                        gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY);
                    } else if (direction == AnimDirection.West) {
                        //voer tween uit
                        rightDown(tileArray[i].world.x, tileArray[i].world.y);
                    }
                    break;
                case -180:
                    if (direction == AnimDirection.North) {
                        //voer tween uit
                        downLeft(tileArray[i].world.x, tileArray[i].world.y);
                    } else if (direction == AnimDirection.East) {
                        //voer tween uit
                        leftDown(tileArray[i].world.x, tileArray[i].world.y);
                    } else if (direction == AnimDirection.South) {
                        //corner angle staat verkeerd om tween te kunnen uitvoeren: ga naar function Gameover
                        gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY);
                    } else if (direction == AnimDirection.West) {
                        //corner angle staat verkeerd om tween te kunnen uitvoeren: ga naar function Gameover
                        gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY);
                    }
                    break;
                case -90:
                    if (direction == AnimDirection.North) {
                        //corner angle staat verkeerd om tween te kunnen uitvoeren: ga naar function Gameover
                        gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY);
                    } else if (direction == AnimDirection.East) {
                        //voer tween uit
                        leftUp(tileArray[i].world.x, tileArray[i].world.y);
                    } else if (direction == AnimDirection.South) {
                        //voer tween uit
                        upLeft(tileArray[i].world.x, tileArray[i].world.y);
                    } else if (direction == AnimDirection.West) {
                        //corner angle staat verkeerd om tween te kunnen uitvoeren: ga naar function Gameover
                        gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY);
                    }
                    break;
                default:
                    //error tween
                    gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY);
                    break;
            }
        }

        function upRight(xPosTween, yPosTween) {
            //circle moving oncomplete
            if (!circleMoving) {
                circleMoving = true;
                //create svg path   
                var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
                //tween point vars
                //var mX = 256; 
                //var mY = 192; 
                //var qX = 256; 
                //var qY = 256;
                //var eX = 320;
                //var eY = 256;
                var mX = xPosTween;
                var mY = yPosTween - 64;
                var qX = xPosTween;
                var qY = yPosTween;
                var eX = xPosTween + 64;
                var eY = yPosTween;
                //create points
                var invulString = "M" + mX + " " + mY + " " + "Q" + qX + " " + qY + " " + eX + " " + eY;
                path.setAttribute('d', invulString)
                //
                var totalPathLength = path.getTotalLength();
                var startPoint = path.getPointAtLength(0)
                //draw the path
                debugPath = game.add.graphics(0, 0)
                debugPath.lineStyle(0, 0xff0000)
                debugPath.moveTo(startPoint.x, startPoint.y)
                //a little tween helper so we can tween a single variable
                tweenHelper = { progress: 0 }
                tweenHelper.onUpdate = function (tween, value) {
                    p = path.getPointAtLength(totalPathLength * value)
                    player.position.x = p.x
                    player.position.y = p.y
                    debugPath.lineTo(p.x, p.y)
                }
                //tween circle
                tween = game.add.tween(tweenHelper).to({ progress: 1 }, 750, "Linear", true, 0).start()
                tween.onUpdateCallback(tweenHelper.onUpdate);
                //after tween, go back to function getNext tile
                tween.onComplete.add(() => GetNextTile(eX, eY, qX, qY));
            }
        }

        function downRight(xPosTween, yPosTween) {
            //circle moving oncomplete
            if (!circleMoving) {
                circleMoving = true;
                //create svg path   
                var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
                //tween point vars
                //var mX = 256;
                //var mY = 320;
                //var qX = 256;
                //var qY = 256;
                //var eX = 320;
                //var eY = 256;
                var mX = xPosTween;
                var mY = yPosTween + 64;
                var qX = xPosTween;
                var qY = yPosTween;
                var eX = xPosTween + 64;
                var eY = yPosTween;
                //create points
                var invulString = "M" + mX + " " + mY + " " + "Q" + qX + " " + qY + " " + eX + " " + eY;
                path.setAttribute('d', invulString)
                //
                var totalPathLength = path.getTotalLength();
                var startPoint = path.getPointAtLength(0)
                //draw the path
                debugPath = game.add.graphics(0, 0)
                debugPath.lineStyle(0, 0xff0000)
                debugPath.moveTo(startPoint.x, startPoint.y)
                //a little tween helper so we can tween a single variable
                tweenHelper = { progress: 0 }
                tweenHelper.onUpdate = function (tween, value) {
                    p = path.getPointAtLength(totalPathLength * value)
                    player.position.x = p.x
                    player.position.y = p.y
                    debugPath.lineTo(p.x, p.y)
                }
                //tween circle
                tween = game.add.tween(tweenHelper).to({ progress: 1 }, 750, "Linear", true, 0).start()
                tween.onUpdateCallback(tweenHelper.onUpdate)
                //after tween, go back to function getNext tile
                tween.onComplete.add(() => GetNextTile(eX, eY, qX, qY));
            }
        }

        function leftDown(xPosTween, yPosTween) {
            //circle moving oncomplete
            if (!circleMoving) {
                circleMoving = true;
                //create svg path   
                var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
                //tween point vars
                //var mX = 192;
                //var mY = 256;
                //var qX = 256;
                //var qY = 256;
                //var eX = 256;
                //var eY = 320;
                var mX = xPosTween - 64;
                var mY = yPosTween;
                var qX = xPosTween;
                var qY = yPosTween;
                var eX = xPosTween;
                var eY = yPosTween + 64;
                //create points
                var invulString = "M" + mX + " " + mY + " " + "Q" + qX + " " + qY + " " + eX + " " + eY;
                path.setAttribute('d', invulString)
                //
                var totalPathLength = path.getTotalLength();
                var startPoint = path.getPointAtLength(0)
                //draw the path
                debugPath = game.add.graphics(0, 0)
                debugPath.lineStyle(0, 0xff0000)
                debugPath.moveTo(startPoint.x, startPoint.y)
                //a little tween helper so we can tween a single variable
                tweenHelper = { progress: 0 }
                tweenHelper.onUpdate = function (tween, value) {
                    p = path.getPointAtLength(totalPathLength * value)
                    player.position.x = p.x
                    player.position.y = p.y
                    debugPath.lineTo(p.x, p.y)
                }
                //tween circle
                tween = game.add.tween(tweenHelper).to({ progress: 1 }, 750, "Linear", true, 0).start()
                tween.onUpdateCallback(tweenHelper.onUpdate)
                //after tween, go back to function getNext tile
                tween.onComplete.add(() => GetNextTile(eX, eY, qX, qY));
            }
        }

        function leftUp(xPosTween, yPosTween) {
            //circle moving oncomplete
            if (!circleMoving) {
                circleMoving = true;
                //create svg path   
                var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
                //tween point vars
                //var mX = 192;
                //var mY = 256;
                //var qX = 256;
                //var qY = 256;
                //var eX = 256;
                //var eY = 192;
                var mX = xPosTween - 64;
                var mY = yPosTween;
                var qX = xPosTween;
                var qY = yPosTween;
                var eX = xPosTween;
                var eY = yPosTween - 64;
                //create points
                var invulString = "M" + mX + " " + mY + " " + "Q" + qX + " " + qY + " " + eX + " " + eY;
                path.setAttribute('d', invulString)
                //
                var totalPathLength = path.getTotalLength();
                var startPoint = path.getPointAtLength(0)
                //draw the path
                debugPath = game.add.graphics(0, 0)
                debugPath.lineStyle(0, 0xff0000)
                debugPath.moveTo(startPoint.x, startPoint.y)
                //a little tween helper so we can tween a single variable
                tweenHelper = { progress: 0 }
                tweenHelper.onUpdate = function (tween, value) {
                    p = path.getPointAtLength(totalPathLength * value)
                    player.position.x = p.x
                    player.position.y = p.y
                    debugPath.lineTo(p.x, p.y)
                }
                //tween circle
                tween = game.add.tween(tweenHelper).to({ progress: 1 }, 750, "Linear", true, 0).start()
                tween.onUpdateCallback(tweenHelper.onUpdate)
                //after tween, go back to function getNext tile
                tween.onComplete.add(() => GetNextTile(eX, eY, qX, qY));
            }
        }

        function rightUp(xPosTween, yPosTween) {
            //circle moving oncomplete
            if (!circleMoving) {
                circleMoving = true;
                //create svg path   
                var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
                //tween point vars
                //var mX = 320;
                //var mY = 256;
                //var qX = 256;
                //var qY = 256;
                //var eX = 256;
                //var eY = 192;
                var mX = xPosTween + 64;
                var mY = yPosTween;
                var qX = xPosTween;
                var qY = yPosTween;
                var eX = xPosTween;
                var eY = yPosTween - 64;
                //create points
                var invulString = "M" + mX + " " + mY + " " + "Q" + qX + " " + qY + " " + eX + " " + eY;
                path.setAttribute('d', invulString)
                //
                var totalPathLength = path.getTotalLength();
                var startPoint = path.getPointAtLength(0)
                //draw the path
                debugPath = game.add.graphics(0, 0)
                debugPath.lineStyle(0, 0xff0000)
                debugPath.moveTo(startPoint.x, startPoint.y)
                //a little tween helper so we can tween a single variable
                tweenHelper = { progress: 0 }
                tweenHelper.onUpdate = function (tween, value) {
                    p = path.getPointAtLength(totalPathLength * value)
                    player.position.x = p.x
                    player.position.y = p.y
                    debugPath.lineTo(p.x, p.y)
                }
                //tween circle
                tween = game.add.tween(tweenHelper).to({ progress: 1 }, 750, "Linear", true, 0).start()
                tween.onUpdateCallback(tweenHelper.onUpdate)
                //after tween, go back to function getNext tile
                tween.onComplete.add(() => GetNextTile(eX, eY, qX, qY));
            }
        }

        function rightDown(xPosTween, yPosTween) {
            //circle moving oncomplete
            if (!circleMoving) {
                circleMoving = true;
                //create svg path   
                var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
                //tween point vars
                //var mX = 320;
                //var mY = 256;
                //var qX = 256;
                //var qY = 256;
                //var eX = 256;
                //var eY = 320;
                var mX = xPosTween + 64;
                var mY = yPosTween;
                var qX = xPosTween;
                var qY = yPosTween;
                var eX = xPosTween;
                var eY = yPosTween + 64;
                //create points
                var invulString = "M" + mX + " " + mY + " " + "Q" + qX + " " + qY + " " + eX + " " + eY;
                path.setAttribute('d', invulString)
                //
                var totalPathLength = path.getTotalLength();
                var startPoint = path.getPointAtLength(0)
                //draw the path
                debugPath = game.add.graphics(0, 0)
                debugPath.lineStyle(0, 0xff0000)
                debugPath.moveTo(startPoint.x, startPoint.y)
                //a little tween helper so we can tween a single variable
                tweenHelper = { progress: 0 }
                tweenHelper.onUpdate = function (tween, value) {
                    p = path.getPointAtLength(totalPathLength * value)
                    player.position.x = p.x
                    player.position.y = p.y
                    debugPath.lineTo(p.x, p.y)
                }
                //tween circle
                tween = game.add.tween(tweenHelper).to({ progress: 1 }, 750, "Linear", true, 0).start()
                tween.onUpdateCallback(tweenHelper.onUpdate)
                //after tween, go back to function getNext tile
                tween.onComplete.add(() => GetNextTile(eX, eY, qX, qY));
            }
        }

        function downLeft(xPosTween, yPosTween) {
            //circle moving oncomplete
            if (!circleMoving) {
                circleMoving = true;
                //create svg path   
                var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
                //tween point vars
                //var mX = 256;
                //var mY = 320;
                //var qX = 256;
                //var qY = 256;
                //var eX = 192;
                //var eY = 256;
                var mX = xPosTween;
                var mY = yPosTween + 64;
                var qX = xPosTween;
                var qY = yPosTween;
                var eX = xPosTween - 64;
                var eY = yPosTween;
                //create points
                var invulString = "M" + mX + " " + mY + " " + "Q" + qX + " " + qY + " " + eX + " " + eY;
                path.setAttribute('d', invulString)
                //
                var totalPathLength = path.getTotalLength();
                var startPoint = path.getPointAtLength(0)
                //draw the path
                debugPath = game.add.graphics(0, 0)
                debugPath.lineStyle(0, 0xff0000)
                debugPath.moveTo(startPoint.x, startPoint.y)
                //a little tween helper so we can tween a single variable
                tweenHelper = { progress: 0 }
                tweenHelper.onUpdate = function (tween, value) {
                    p = path.getPointAtLength(totalPathLength * value)
                    player.position.x = p.x
                    player.position.y = p.y
                    debugPath.lineTo(p.x, p.y)
                }
                //tween circle
                tween = game.add.tween(tweenHelper).to({ progress: 1 }, 750, "Linear", true, 0).start()
                tween.onUpdateCallback(tweenHelper.onUpdate)
                //after tween, go back to function getNext tile
                tween.onComplete.add(() => GetNextTile(eX, eY, qX, qY));
            }
        }

        function upLeft(xPosTween, yPosTween) {
            //circle moving oncomplete
            if (!circleMoving) {
                circleMoving = true;
                //create svg path   
                var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
                //tween point vars
                //var mX = 256;
                //var mY = 192;
                //var qX = 256;
                //var qY = 256;
                //var eX = 192;
                //var eY = 256;
                var mX = xPosTween;
                var mY = yPosTween - 64;
                var qX = xPosTween;
                var qY = yPosTween;
                var eX = xPosTween - 64;
                var eY = yPosTween;
                //create points
                var invulString = "M" + mX + " " + mY + " " + "Q" + qX + " " + qY + " " + eX + " " + eY;
                path.setAttribute('d', invulString)
                //
                var totalPathLength = path.getTotalLength();
                var startPoint = path.getPointAtLength(0)
                //draw the path
                debugPath = game.add.graphics(0, 0)
                debugPath.lineStyle(0, 0xff0000)
                debugPath.moveTo(startPoint.x, startPoint.y)
                //a little tween helper so we can tween a single variable
                tweenHelper = { progress: 0 }
                tweenHelper.onUpdate = function (tween, value) {
                    p = path.getPointAtLength(totalPathLength * value)
                    player.position.x = p.x
                    player.position.y = p.y
                    debugPath.lineTo(p.x, p.y)
                }
                //tween circle
                tween = game.add.tween(tweenHelper).to({ progress: 1 }, 750, "Linear", true, 0).start()
                tween.onUpdateCallback(tweenHelper.onUpdate)
                //after tween, go back to function getNext tile
                tween.onComplete.add(() => GetNextTile(eX, eY, qX, qY));
            }
        }

        function gameOver(xPosCircle, yPosCircle, nextTileX, nextTileY) {
            //tween circle gameover: fx + tween circle + show overlay
            game.add.tween(player).to({ x: nextTileX, y: nextTileY }, 2000, 'Linear', true, 0).start();
            game.add.tween(player.scale).to({ x: 0.5, y: 0.5 }, 1000, Phaser.Easing.Linear.None, true, 150);
            game.add.tween(player).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true, 150);
            //ga naar loseOverlay
            setTimeout(loseOverlay, 1250);
            gameOverSound.play();
        }

        function boardEdgeGameOver(xPosCircle, yPosCircle) {
            //tween circle gameover: fx + tween circle + show overlay
            game.add.tween(player.scale).to({ x: 0.5, y: 0.5 }, 1000, Phaser.Easing.Linear.None, true, 150);
            game.add.tween(player).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true, 150);
            //ga naar loseOverlay
            setTimeout(loseOverlay, 1250);
            //gameover audio
            gameOverSound.play();
        }

        function loseOverlay() {
            //add retryoverlay
            retryOverlay = game.add.sprite(0, 0, 'loseBackground');
            retryOverlay.anchor.set(0, 0)
            retryOverlay.alpha = 0;
            //alpha tween
            game.add.tween(retryOverlay).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
            //add retrybutton
            retryButton = game.add.button(768, 640, 'nextButton', overlayTween, this, 0, 0, 1);
            retryButton.anchor.setTo(0.5, 0.5);
            retryButton.input.useHandCursor = true;
            retryButton.alpha = 0;
            //alpha tween
            game.add.tween(retryButton).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
            //corners niet clickable bij show retryoverlay
            for (i = 0; i < tileArray.length ; i++) {
                tileArray[i].inputEnabled = false;
            }
            //create level + score text bij lose overlay
            levelText = game.add.text(game.world.centerX, game.world.centerY - 200, 'Je hebt level ' + level + ' niet gehaald!', { fontSize: '25px', fill: '#50514F' });
            levelText.anchor.setTo(0.5, 0.5);
            levelText.alpha = 0;
            scoreText = game.add.text(game.world.centerX, game.world.centerY - 150, 'Je score is 0', { fontSize: '25px', fill: '#50514F' });
            scoreText.anchor.setTo(0.5, 0.5);
            scoreText.alpha = 0;
            bonusScoreText = game.add.text(game.world.centerX, game.world.centerY - 50, 'Je bonus score is ' + score2, { fontSize: '25px', fill: '#50514F' });
            bonusScoreText.anchor.setTo(0.5, 0.5);
            bonusScoreText.alpha = 0;
            //var input = game.add.inputField(10, 90);
            //alpha tween
            game.add.tween(levelText).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
            game.add.tween(scoreText).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
            game.add.tween(bonusScoreText).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
        }

        function overlayTween() {
            //button click audio
            buttonSound.play();
            //transition properties
            game.stateTransition.configure({
                duration: Phaser.Timer.SECOND * 0.5,
                ease: Phaser.Easing.Linear.InOut,
                properties: {
                    alpha: 0,
                }
            });
            var userName = prompt("Voer je naam in", "");
            var errorText;
            if (userName) {
                console.log("Hello " + userName);

                var newScore = Number(0);
                if (localStorage.getItem('score') != null) {
                    newScore = Number(localStorage.getItem('score'));
                } else {
                    localStorage.setItem('score', 0);
                }
                game.state.states['LeaderBoard'].score = Number(newScore);
                localStorage.setItem('score', Number(newScore));

                //haal json van scoreArray op
                var highScoresJson;
                var highScoresArray = [];
                if (localStorage.getItem('highScores') != null) {
                    highScoresJson = localStorage.getItem('highScores');
                    //maak array van json (scoreArray)
                    highScoresArray = JSON.parse(highScoresJson);
                }
                highScoresArray.push(Number(localStorage.getItem('score')));

                //highScoresArray.push({ score: Number(localStorage.getItem('score')), userName: 'username' });

                //voeg huidige score toe aan array.

                //maak json van array
                //schrijf json naar ??? localstorage?
                highScoresJson = "";
                highScoresJson = JSON.stringify(highScoresArray);
                localStorage.setItem('highScores', highScoresJson);
                //Toon array in leaderboard ipv/en score

                game.stateTransition.to('LeaderBoard', true, false);
            }
            else {
                errorText = game.add.text(game.world.centerX + 150, game.world.centerY + 150, 'Voer je naam in', { fontSize: '20px', fill: '#F00' });
                errorText.alpha = 0;
                game.add.tween(errorText).to({ alpha: 1 }, 500, Phaser.Easing.Linear.None, true, 0);
                errorText.anchor.setTo(0.5, 0.5);
            }
        }

        function levelComplete(xPosCircle, yPosCircle) {
            //tween circle: fx + tween circle + show overlay
            scoreCounter();
            //var circle;
            //circle = game.add.sprite(xPosCircle, yPosCircle, 'Circle');
            //circle.anchor.set(0.5)
            game.add.tween(player).to({ x: 768, y: 700 }, 2000, 'Linear', true, 0).start();
            game.add.tween(player.scale).to({ x: 0.5, y: 0.5 }, 1000, Phaser.Easing.Linear.None, true, 150);
            game.add.tween(player).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true, 150);
            //ga naar winOverlay
            winOverlay();
        }

        function winOverlay() {
            //winsound play
            winSound.play();
            //add retryoverlay
            retryOverlay = game.add.sprite(0, 0, 'winBackground');
            retryOverlay.anchor.set(0, 0)
            retryOverlay.alpha = 0;
            //alpha tween
            game.add.tween(retryOverlay).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
            //add retrybutton
            playButton = game.add.button(768, 640, 'playButtonSmall', nextLevel, this, 0, 0, 1);
            playButton.anchor.setTo(0.5, 0.5);
            playButton.input.useHandCursor = true;
            playButton.alpha = 0;
            //aplha tween
            game.add.tween(playButton).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
            //corners niet clickable bij show retryoverlay
            for (i = 0; i < tileArray.length ; i++) {
                tileArray[i].inputEnabled = false;
            }
            //create level + score tekst win
            levelText = game.add.text(game.world.centerX, game.world.centerY - 200, 'Je hebt level ' + level + ' gehaald!', { fontSize: '25px', fill: '#50514F' });
            levelText.anchor.setTo(0.5, 0.5);
            levelText.alpha = 0;
            scoreText = game.add.text(game.world.centerX, game.world.centerY - 150, 'Je score is ' + score, { fontSize: '25px', fill: '#50514F' });
            scoreText.anchor.setTo(0.5, 0.5);
            scoreText.alpha = 0;
            bonusScoreText = game.add.text(game.world.centerX, game.world.centerY - 50, 'Je bonus score is ' + score2, { fontSize: '25px', fill: '#50514F' });
            bonusScoreText.anchor.setTo(0.5, 0.5);
            bonusScoreText.alpha = 0;
            //alpha tween
            game.add.tween(levelText).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
            game.add.tween(scoreText).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
            game.add.tween(bonusScoreText).to({ alpha: 1 }, 1000, Phaser.Easing.Linear.None, true, 1000);
        }

        function scoreCounter() {
            //Add and update the score
            score = Number(localStorage.getItem('score')) + Number(1);
            scoreText.setText("Score " + score);
            localStorage.setItem('score', Number(score));
            //corner.kill();
        }

        function nextLevel() {
            //button click audio
            buttonSound.play();
            //alpha tween
            game.add.tween(retryOverlay).to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true);
            //ga naar nextlevelstate
            nextLevelState();
        }

        function nextLevelState() {
            //transition properties
            game.stateTransition.configure({
                duration: Phaser.Timer.SECOND * 0.5,
                ease: Phaser.Easing.Linear.InOut,
                properties: {
                    alpha: 0,
                    scale: {
                        x: 1.4,
                        y: 1.4
                    }
                }
            });
            //ga naar level02
            game.stateTransition.to('Level02', true, false);
        }
    },

    update: function () {
        this.game.physics.arcade.overlap(player, bonusGroup, collectBonus, null, this);

        function collectBonus(player, bonus) {
            // Removes the bonus from the screen
            //this.game.add.tween(bonus).to({ alpha: 0 }, 20, Phaser.Easing.Linear.None, true);
            bonus.kill();
            winSound.play();
            //  Add and update the score   
            score2 += 1;
            scoreText2.text = 'Bonus score ' + score2;
            totaalScore.text = 'Totaal score ' + score + ' ' + score2;

            
        }
    },

    render: function () { },
};