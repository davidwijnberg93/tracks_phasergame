window.onload = function () {

    var game = new Phaser.Game(1024, 768, Phaser.AUTO, 'Tracks');

    //  Add the States your game has.
    game.state.add('Boot', GameStates.Boot);
    game.state.add('Preloader', GameStates.Preloader);
    game.state.add('MainMenu', GameStates.MainMenu);
    game.state.add('Level01', GameStates.Level01);
    game.state.add('Level02', GameStates.Level02);
    game.state.add('LeaderBoard', GameStates.LeaderBoard);

    //  Now start the Boot state.
    game.state.start('Boot');

};