﻿// Boot will take care of initializing a few settings,
// declare the object that will hold all game states

var GameStates = {
    //quite common to add game variables/constants in here

};

GameStates.Boot = function (game) {  //declare the boot state

};

GameStates.Boot.prototype = {
    preload: function () {
        // load assets to be used later in the preloader e.g. for loading screen / splashscreen
        this.load.image('preloaderBar', 'assets/preloader-bar.png');
    },
    create: function () {
        // setup game environment
        // scale, input etc..
        this.stage.backgroundColor = '#FFFFFF';
        //game transition states plugin
        this.game.stateTransition = this.game.plugins.add(Phaser.Plugin.StateTransition);
        // call next state
        this.state.start('Preloader');
    }
};

