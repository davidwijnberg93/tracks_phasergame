﻿// Preloader will load all of the assets like graphics and audio
GameStates.Preloader = function (game) {
    this.preloadBar = null;
}

GameStates.Preloader.prototype = {
    preload: function () {
        // common to add a loading bar sprite here...
        this.preloadBar = this.add.sprite(this.game.width / 2 - 100, this.game.height / 2, 'preloaderBar');
        this.load.setPreloadSprite(this.preloadBar);
        // load all game assets
        // images, spritesheets, atlases, audio etc..
        this.load.image('logo', 'assets/phaser2.png');

        this.load.spritesheet('Tile', 'assets/sprites/tile2.png', 128, 128);
        this.load.spritesheet('levelSprite', 'assets/sprites/levelsprite.png', 158, 158);
        this.load.image('Straight', 'assets/sprites/straight2.png', 128, 128);
        this.load.image('Corner', 'assets/sprites/corner5.png', 128, 128);
        this.load.image('Circle', 'assets/sprites/circle2.png', 64, 64);
        this.load.image('Bonus', 'assets/sprites/bonus2.png', 32, 28);
        this.load.image('Home', 'assets/sprites/begin3.png', 128, 128);
        this.load.image('Finish', 'assets/sprites/eind4.png', 128, 128);
        this.load.spritesheet('playButton', 'assets/sprites/playbutton2.png', 256, 256);
        this.load.spritesheet('playButtonSmall', 'assets/sprites/playbuttonklein.png', 128, 128);
        this.load.spritesheet('retryButton', 'assets/sprites/retrybutton.png', 128, 128);
        this.load.spritesheet('menuButton', 'assets/sprites/menubutton.png', 128, 128);
        this.load.spritesheet('nextButton', 'assets/sprites/nextbutton.png', 128, 128);
        this.load.spritesheet('previousButton', 'assets/sprites/previousbutton.png', 128, 128);
        this.load.spritesheet('leaderBoardButton', 'assets/sprites/leaderboardbutton.png', 128, 128);
        this.load.image('Background', 'assets/sprites/background3.png', 1024, 768);
        this.load.image('mainMenuBackground', 'assets/sprites/mainmenubackground2.png', 1024, 768);
        this.load.image('leaderBoardBackground', 'assets/sprites/leaderboardbackground4.png', 1024, 768);
        this.load.image('loseBackground', 'assets/sprites/losebackground3.png', 1024, 768);
        this.load.image('winBackground', 'assets/sprites/winbackground2.png', 1024, 768);
        this.load.image('beginOverlay', 'assets/sprites/beginoverlay.png', 1024, 768);
        this.load.image('winOverlay', 'assets/sprites/winoverlay.png', 1024, 768);
        this.load.image('retryOverlay', 'assets/sprites/retryoverlay.png', 1024, 768);
        this.load.image('Level01', 'assets/sprites/level01.png', 128, 64);
        this.load.image('Level02', 'assets/sprites/level02.png', 128, 64);
        this.load.audio('buttonSound', 'assets/audio/button.wav');
        this.load.audio('countdownSound', 'assets/audio/countdown.wav');
        this.load.audio('cornerRotateSound', 'assets/audio/menu_switch.mp3');
        this.load.audio('gameOverSound', 'assets/audio/numkey.wav');
        this.load.audio('winSound', 'assets/audio/numkey.wav');
    },

    create: function () {
        //call next state
        this.state.start('MainMenu');
    }
};